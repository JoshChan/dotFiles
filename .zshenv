alias ls='ls --color=auto'
alias ll='ls -la'
alias lsd='ls -lad */'
alias cls='printf "\033c"'
alias tree='tree /a'
alias ahk='"C:/Program Files/AutoHotkey/AutoHotKey.exe"'
alias pip='python -m pip'
alias py3='python3'
alias pip3='python3 -m pip'
alias stfu='taskkill /f'

