"==============================================================================
" Plugins 
"------------------------------------------------------------------------------

" Pathogen default dir
execute pathogen#infect()
" Enables plugin commands
filetype plugin on

" Airline Config  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
" Causes Airline to always appear. Default is only when a split is open.
set laststatus=2

" Syntastic Config   -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_check_on_w = 0
" Syntastic Checker Selection  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
"let g:syntastic_css_checkers = ['syntastic-checkers-css']
"let g:syntastic_html_checkers = ['syntastic-checkers-html']
let g:syntastic_java_checkers = ['javac']
"let g:syntastic_javascript_checkers = ['syntastic-checkers-javascript']
let g:syntastic_lua_checkers = ['luac']
"let g:syntastic_python_checkers = ['flake8']
let g:syntastic_python_checkers = []
"let g:syntastic_xml_checkers = ['syntastic-checkers-xml']
"let g:syntastic_zsh_checkers = ['syntastic-checkers-zsh']

"==============================================================================
" Settings
"------------------------------------------------------------------------------

set relativenumber
set number

" {{{
set scrolloff=1 " Show at least one line above/below cursor.
set smartindent
set showmatch
set wildmenu
set incsearch
" set foldcolumn=1
" }}

set splitright
set splitbelow

set shiftwidth=4
let &softtabstop = &shiftwidth
set expandtab
" set tabstop=4
set autoindent
set textwidth=79

set ts=4
set spell spelllang=en_us
set nospell
syntax on

" Cursor highlights
" NOTE: ctermbg is cool, but I gotta check out the rest.
set cursorline
highlight CursorLine cterm=NONE ctermbg=black


set ttymouse=xterm2
set mouse+=a


"==============================================================================
" Key Bindings 
"------------------------------------------------------------------------------

nnoremap <C-H> <C-W><C-H>
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>

" Quick no highlight
" TODO: Fix - Starts Vim in some kind of replacement.
"B
"nnoremap <silent> <esc> :<c-u>noh<return><esc>

nnoremap <cr> :

nnoremap -w :w<enter>

nnoremap -ev :e ~/.vimrc<cr>
nnoremap -sv :source ~/.vimrc<cr>

nnoremap -p "+p
nnoremap -y "+y 
vnoremap -y "+y
vnoremap -p "+p

"==============================================================================
" Commands
"------------------------------------------------------------------------------

" Reload Vim
command! RELOADVIM :so ~/.vimrc

" Edit .vimrc
command! EDITVIMRC :e ~/.vimrc

" Open my To do
command! TODO :e ~/todo.txt

" Open my 'Sup"
command! SUP :e ~/sup.txt

" Record the current date
command! LOG :execute "normal! o\<END>"|execute "read!date"

" Why not
command! BYE :qa
command! BAIL :qa!

" Control clipboard 'modes'.
command! SYNCPASTE set clipboard=unnamed
command! FREEPASTE set clipboard="*
-
" Scratch buffer
command! SC vnew | setlocal nobuflisted buftype=nofile bufhidden=wipe noswapfile
"==============================================================================
" Autocommands
"------------------------------------------------------------------------------

